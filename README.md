# NSS ukol

## Autoři

 - Long Hoang
 

## Použité technologie

 - GitLab web UI
 - Git Bash

## Popis

Tento úkol byl zadán v rámci přemětu NSS.

## Instalace

 - Naklonujte repozitář do svého zařízení
 - Hotovo

## Copyright

Viz license.md
